import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GeneratorService {

  constructor() { }

  data:Data = {
    "head": {"size": 10, "values": {
        "1": "Casque", 
        "2": "Masque", 
        "3": "Capuche", 
        "4": "Cheveux Courts", 
        "5": "Cheveux Longs", 
        "6": "Chauve", 
        "7": "Chapeau", 
        "8": "Cagoule de cultiste", 
        "9": "Cornes", 
        "10": "Rouflaquettes"
    }},
    "eyes": {"size": 10, "values": {
        "1": "Lunettes", 
        "2": "Cache-Oeil", 
        "3": "Bandeau", 
        "4": "Borgne", 
        "5": "Cicatrices", 
        "6": "Tout va bien", 
        "7": "Aveugle", 
        "8": "Troisième Oeil", 
        "9": "Maquillés", 
        "10": "Multitude"
    }},
    "face": {"size": 12, "values": {
        "1": "Barbre Courte", 
        "2": "Longue Barbe", 
        "3": "Glabre", 
        "4": "Hirsute", 
        "5": "Cicatrice aux lèvres", 
        "6": "Poupon", 
        "7": "Bestial", 
        "8": "Défiguré", 
        "9": "Démoniaque", 
        "10": "Grande Gueule", 
        "11": "Tatoué", 
        "12": "Moustache"
    }},
    "clothes": {"size": 12, "values": {
        "1": "Nu", 
        "2": "Guenilles", 
        "3": "Haillons Pourris", 
        "4": "Robes", 
        "5": "Armure Légère", 
        "6": "Armure Moyenne", 
        "7": "Armure Lourde", 
        "8": "Ossements", 
        "9": "Ostentatoires", 
        "10": "Pagne", 
        "11": "Végétaux", 
        "12": "Costume Cérémonial"
    }},
    "weapon": {"size": 100, "values": {
        "3": "Arme improvisée", 
        "9": "Bâton", 
        "15": "Dague", 
        "21": "Epée", 
        "27": "Hache", 
        "30": "Morgenstern", 
        "33": "Nadziak", 
        "36": "Fléau", 
        "39": "Fronde", 
        "42": "Sac de rats", 
        "45": "Cestes", 
        "48": "Fléau d'Os", 
        "51": "Filet", 
        "54": "Grenade Impie d'Apemeia", 
        "57": "Epée Courte", 
        "60": "Marteau de guerre", 
        "63": "Rapière", 
        "66": "Flamberge", 
        "69": "Grande Hache", 
        "72": "Hallebarde", 
        "75": "Lance", 
        "78": "Trident", 
        "81": "Arquebuse", 
        "84": "Mousquet", 
        "87": "Grosse Arme Improvisée", 
        "90": "Arc", 
        "94": "Arbalète", 
        "97": "Epée Batarde", 
        "99": "Couleuvrine", 
        "100": "Choisissez !"
    }},
    "more": {"size": 20, "values": {
        "1": "Symbole religieux", 
        "2": "Membre amputé", 
        "3": "Main prosthétique", 
        "4": "Chaines", 
        "5": "Arme supplémentaire", 
        "6": "Mohawk", 
        "7": "Livres", 
        "8": "Bannière", 
        "9": "Transporte des bouts de cadavre", 
        "10": "Tête étrange", 
        "11": "Sanglant", 
        "12": "Sans visage", 
        "13": "Cordages", 
        "14": "Bras tentaculaire", 
        "15": "Cicatrices", 
        "16": "Ventre distendu", 
        "17": "Maladie de peau", 
        "18": "Est-il encore en vie ?", 
        "19": "Yeux luminescents", 
        "20": "Queue"
    }},
    "light": {"size": 12, "values": {
        "1": "Lanterne", 
        "2": "Torche", 
        "3": "Bougie", 
        "4": "Champignon", 
        "5": "Familier", 
        "6": "Pierre Incandescente", 
        "7": "Feu Follet", 
        "12": "Rien"
    }},
    "shield": {"size": 2, "values": {
        "1": "Oui",
        "2": "Non"
    }},
    "potion": {"size": 2, "values": {
        "1": "Oui",
        "2": "Non"
    }},
    "bag": {"size": 2, "values": {
        "1": "Oui",
        "2": "Non"
    }}
};

  public newWarrior(sorcerer:boolean): any {
    let warrior:Warrior = {};
    for(let k in this.data) {
      const dSize = this.data[k].size;
      const n = Math.ceil(Math.random() * dSize);
      let value;
      for(let i in this.data[k].values) {
        const max = parseInt(i);
        if(n <= max) {
          value = i;
          break;
        }
      }

      if(value) {
        warrior[k] = value;
      }      
    }

    if(sorcerer) {
      warrior['sorcerer'] = true;
      warrior['shield'] = "2";
      if(warrior['clothes'] === "7") {
        warrior['clothes'] = "6";
      }
    }

    return warrior;
  }
}

export interface Data {
  [key: string]: DataItem;
}

export interface DataItem {
  size:      number;
  values:     DataItemValue;
}

export interface DataItemValue {
  [key: string]: string;
}

export interface Warrior {
  [key: string]: string | boolean;
}
