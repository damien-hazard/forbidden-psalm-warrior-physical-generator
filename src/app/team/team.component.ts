import { Component, OnInit } from '@angular/core';
import { GeneratorService, Warrior } from '../generator.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.scss']
})
export class TeamComponent implements OnInit {

  caracs:Carac[] = [
    { name: "head", label: "Tête" },
    { name: "eyes", label: "Yeux" },
    { name: "face", label: "Visage" },
    { name: "clothes", label: "Vêtements" },
    { name: "weapon", label: "Arme" },
    { name: "more", label: "Le truc en plus" },
    { name: "light", label: "Lumière" },
    { name: "shield", label: "Bouclier" },
    { name: "potion", label: "Potion" },
    { name: "bag", label: "Sac" },
    { name: "sorcerer", label: "Lanceur de sorts" },
  ]

  team:Warrior[] = Array();
  generatedAt:Date = new Date();

  constructor(
    private generatorService: GeneratorService,
    private datepipe: DatePipe) {}

  ngOnInit(): void {
    console.log("init team", sessionStorage.getItem("team"))
    if(sessionStorage.getItem("team")) {
      this.team = JSON.parse(sessionStorage.getItem("team") || '[]');
    }
  }

  private saveTeam():void {
    sessionStorage.setItem("team", JSON.stringify(this.team));
  }

  addWarrior(sorcerer:boolean):void {
    const w = this.generatorService.newWarrior(sorcerer);
    console.log("addWarrior", w);
    this.team.push(w);
    this.saveTeam();
    this.generatedAt = new Date();
  }

  removeWarrior(w:Warrior):void {
    let i = this.team.indexOf(w);
    console.log("remove warrior", i, w);
    if(i >= 0) {
      this.team.splice(i, 1);
      this.saveTeam();
    }
  }

  reset():void {
    this.team.splice(0, this.team.length);
    this.saveTeam();
  }

  print():void {
    window.print();
    this.generatedAt = new Date();
  }  

  export():void {
    let buffer = '';
    for(let carac of this.caracs) {
      buffer += carac.label + ';'
    }
    buffer += "\r\n";
    for(let warrior of this.team) {
      for(let carac of this.caracs) {
        buffer += (this.getLabel(carac.name, warrior[carac.name]) || '-') + ';'
      }
      buffer += "\r\n";
    }

    const filename = 'forbidden-psalm-band-'+ this.datepipe.transform(this.generatedAt, 'yyyy-MM-dd_hh-mm', 'CET') +'.csv';
    const blob: Blob = new Blob([buffer], { type: 'text/csv;charset=utf-8;' });
      
    const link = document.createElement('a');
    if (link.download !== undefined) {
      // Browsers that support HTML5 download attribute
      const url = URL.createObjectURL(blob);
      link.setAttribute('href', url);
      link.setAttribute('download', filename);
      link.style.visibility = 'hidden';
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    }
  }

  getLabel(type:string, value:string | boolean): string {
    if(typeof(value) === 'boolean') {
      return value ? "Oui" : "Non";
    }
    return this.generatorService.data[type]?.values[value];
  }

  hasSorcerer(): boolean {
    for(let w of this.team) {
      if(w['sorcerer'] === true) {
        return true;
      }
    }
    return false;
  }

}

interface Carac {
  name: string,
  label: string
}